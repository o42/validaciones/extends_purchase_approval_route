# -*- coding: utf-8 -*-
{
    'name': 'Extends PO Validaciones (purchase_approval_route)',
    'version': '1.0.0',
    'summary': """
    Agrega validaciones y catalgo de validadores para pedidos de compras
    """,
    'category': 'Purchases',
    'author': 'Odoo',
    'support': 'odoo',
    'license': 'OPL-1',
    'description':
        """
Purchase Order Approval Cycle
=============================
This module helps to create multiple custom, flexible and dynamic approval route
for purchase orders based on purchase team settings.
We integrate the same functions for the purchase, request and requisition, we added a check box for select only one
and integrate for the orders.
        """,
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_team_views.xml',
        'views/purchase_order_views.xml',
        'views/catalog_roles_views.xml',
    ],
    'depends': ['purchase_approval_route'],
    'installable': True,
    'auto_install': True,
    'application': False,
}
