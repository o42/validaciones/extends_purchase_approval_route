# -*- coding: utf-8 -*-
from odoo import api, fields, models


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    current_approver = fields.Many2one(store=True)

    @api.onchange("company_id")
    def onchangue_company_id(self):
        if self.po_order_approval_route != "no" and not self.requisition_id:
            team_id = self.env['purchase.team'].search([
                ("company_id", "=", self.company_id.id),
                ("user_id", "=", self.user_id.id),
                ("active", "=", True),
                ("is_purchase", "=", True)
            ], limit=1)
            if team_id:
                self.team_id = team_id.id
