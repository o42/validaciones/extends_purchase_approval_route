# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import ValidationError


class PurchaseTeam(models.Model):
    _inherit = "purchase.team"

    is_purchase = fields.Boolean(string="is purchase order?", )
    domain_role_ids = fields.Many2many('catalog.roles', )

    @api.onchange("is_purchase")
    def onchange_type_fill_domain_role_ids(self):
        if self.is_purchase:
            domain = self.env["catalog.roles"].search([
                ("is_purchase", "=", True),
                ("active", "=", True)
            ]).mapped("id")
            self.domain_role_ids = [(4, id, 0) for id in domain]
        else:
            self.domain_role_ids = [(5, 0, 0)]

    @api.constrains('user_id', 'is_purchase')
    def _check_description(self):
        for record in self:
            teams = record.env["purchase.team"].search([
                ("active", "=", True),
                ("user_id", "=", record.user_id.id),
                ("is_purchase", "=", True)
            ])
            if len(teams) > 1:
                raise ValidationError("The selected user already has an active validation string assigned")
