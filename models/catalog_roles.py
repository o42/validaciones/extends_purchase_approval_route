# -*- coding: utf-8 -*-
from odoo import fields, models


class CatalogRoles(models.Model):
    _name = "catalog.roles"

    name = fields.Char(string="name")
    is_excluded = fields.Boolean(string="excluded for range?", )
    is_purchase = fields.Boolean(string="is for purchase?", )
    active = fields.Boolean(string="Active", default=True)
