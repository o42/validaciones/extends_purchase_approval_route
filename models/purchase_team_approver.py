# -*- coding: utf-8 -*-
from odoo import api, fields, models


class PurchaseTeamApprover(models.Model):
    _inherit = "purchase.team.approver"

    role_id = fields.Many2one(
        comodel_name='catalog.roles', string='Role',
        index=True
    )
    can_edit = fields.Boolean(related='role_id.is_excluded', )

    @api.onchange("role_id")
    def onchange_role_id(self):
        for record in self:
            if record.role_id:
                record.role = record.role_id.name
